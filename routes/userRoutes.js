const express = require('express');
const router = express.Router();
const userController = require ('../controllers/userController');
const auth = require("../auth");

// Route For user Registration
router.post("/register", (req,res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});

// Route For User Loin Authentication
router.post("/login", (req, res)=> {
	userController.loginUSer(req.body).then(resultFromController => res.send(resultFromController))
});

// Checking Email
router.post("/checkEmail", (req,res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController)) 
});

// Retrieve User details
router.post("/details", auth.verify,(req, res) =>{
	const userData = auth.decode(req.headers.authorization);
	console.log(userData)
	console.log(req.headers.authorization)
	
	userController.getProfile({id: userData.id}).then(resultFromController => res.send(resultFromController))
});





module.exports = router;
