const express = require('express');
const router = express.Router();
const orderController = require('../controllers/orderController');
const userController = require('../controllers/userController');
const auth = require("../auth");

// Controller Functions

// Create Order
router.post("/addOrder",auth.verify,(req, res)=>{
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin === false){
		orderController.addOrder(req.body).then(resultFromController => res.send(resultFromController))
	}else {
		res.send({auth : "You are an admin"})
	}
})




module.exports = router;